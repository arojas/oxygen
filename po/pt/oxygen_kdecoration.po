# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: breeze_kwin_deco\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-13 00:58+0000\n"
"PO-Revision-Date: 2017-12-13 10:40+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: ms Oxygen\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: config/oxygenanimationconfigwidget.cpp:31
#, kde-format
msgid "Button mouseover transition"
msgstr "Transição à passagem do rato nos botões"

#: config/oxygenanimationconfigwidget.cpp:31
#, kde-format
msgid "Configure window buttons' mouseover highlight animation"
msgstr ""
"Configurar a animação de realce à passagem do cursor para os botões da janela"

#: config/oxygenanimationconfigwidget.cpp:36
#, kde-format
msgid "Window active state change transitions"
msgstr "Transições de mudança de estado da janela activa"

#: config/oxygenanimationconfigwidget.cpp:37
#, kde-format
msgid ""
"Configure fading between window shadow and glow when window's active state "
"is changed"
msgstr ""
"Configurar a transição desvanecida entre a sombra e o brilho de uma janela, "
"quando muda o estado de actividade da mesma"

#: config/oxygenexceptionlistwidget.cpp:88
#, kde-format
msgid "New Exception - Oxygen Settings"
msgstr "Nova Excepção - Configuração do Oxygen"

#: config/oxygenexceptionlistwidget.cpp:135
#, kde-format
msgid "Edit Exception - Oxygen Settings"
msgstr "Editar a Excepção - Configuração do Oxygen"

#: config/oxygenexceptionlistwidget.cpp:167
#, kde-format
msgid "Question - Oxygen Settings"
msgstr "Pergunta - Configuração do Oxygen"

#: config/oxygenexceptionlistwidget.cpp:168
#, kde-format
msgid "Remove selected exception?"
msgstr "Deseja remover a excepção seleccionada?"

#. i18n: ectx: property (text), widget (QPushButton, removeButton)
#: config/oxygenexceptionlistwidget.cpp:170
#: config/ui/oxygenexceptionlistwidget.ui:79
#, kde-format
msgid "Remove"
msgstr "Remover"

#: config/oxygenexceptionlistwidget.cpp:302
#, kde-format
msgid "Warning - Oxygen Settings"
msgstr "Atenção - Configuração do Oxygen"

#: config/oxygenexceptionlistwidget.cpp:302
#, kde-format
msgid "Regular Expression syntax is incorrect"
msgstr "A sintaxe da expressão regular está incorrecta"

#: config/oxygenexceptionmodel.cpp:18
#, kde-format
msgid "Exception Type"
msgstr "Tipo de Excepção"

#: config/oxygenexceptionmodel.cpp:18
#, kde-format
msgid "Regular Expression"
msgstr "Expressão Regular"

#. i18n: ectx: property (text), item, widget (QComboBox, exceptionType)
#: config/oxygenexceptionmodel.cpp:36 config/ui/oxygenexceptiondialog.ui:72
#, kde-format
msgid "Window Title"
msgstr "Título da Janela"

#. i18n: ectx: property (text), item, widget (QComboBox, exceptionType)
#: config/oxygenexceptionmodel.cpp:40 config/ui/oxygenexceptiondialog.ui:67
#, kde-format
msgid "Window Class Name"
msgstr "Nome da Classe da Janela"

#: config/oxygenexceptionmodel.cpp:55
#, kde-format
msgid "Enable/disable this exception"
msgstr "Activar/desactivar esta excepção"

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: config/ui/oxygenconfigurationui.ui:24
#, kde-format
msgid "General"
msgstr "Geral"

#. i18n: ectx: property (text), item, widget (QComboBox, buttonSize)
#: config/ui/oxygenconfigurationui.ui:31
#, kde-format
msgctxt "@item:inlistbox Button size:"
msgid "Small"
msgstr "Pequeno"

#. i18n: ectx: property (text), item, widget (QComboBox, buttonSize)
#: config/ui/oxygenconfigurationui.ui:36
#, kde-format
msgctxt "@item:inlistbox Button size:"
msgid "Normal"
msgstr "Normal"

#. i18n: ectx: property (text), item, widget (QComboBox, buttonSize)
#: config/ui/oxygenconfigurationui.ui:41
#, kde-format
msgctxt "@item:inlistbox Button size:"
msgid "Large"
msgstr "Grande"

#. i18n: ectx: property (text), item, widget (QComboBox, buttonSize)
#: config/ui/oxygenconfigurationui.ui:46
#, kde-format
msgctxt "@item:inlistbox Button size:"
msgid "Very Large"
msgstr "Muito Grande"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: config/ui/oxygenconfigurationui.ui:67
#, kde-format
msgid "Tit&le alignment:"
msgstr "A&linhamento do título:"

#. i18n: ectx: property (text), item, widget (QComboBox, titleAlignment)
#: config/ui/oxygenconfigurationui.ui:81
#, kde-format
msgid "Left"
msgstr "Esquerda"

#. i18n: ectx: property (text), item, widget (QComboBox, titleAlignment)
#: config/ui/oxygenconfigurationui.ui:86
#, kde-format
msgid "Center"
msgstr "Centro"

#. i18n: ectx: property (text), item, widget (QComboBox, titleAlignment)
#: config/ui/oxygenconfigurationui.ui:91
#, kde-format
msgid "Center (Full Width)"
msgstr "Centro (Largura Total)"

#. i18n: ectx: property (text), item, widget (QComboBox, titleAlignment)
#: config/ui/oxygenconfigurationui.ui:96
#, kde-format
msgid "Right"
msgstr "Direita"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: config/ui/oxygenconfigurationui.ui:104
#, kde-format
msgid "B&utton size:"
msgstr "Tamanho dos &botões:"

#. i18n: ectx: property (text), widget (QCheckBox, drawSizeGrip)
#: config/ui/oxygenconfigurationui.ui:117
#, kde-format
msgid "Add handle to resize windows with no border"
msgstr "Adicionar uma pega para dimensionar as janelas sem contorno"

#. i18n: ectx: property (text), widget (QCheckBox, drawBorderOnMaximizedWindows)
#: config/ui/oxygenconfigurationui.ui:124
#, kde-format
msgid "Allow resizing maximized windows from window edges"
msgstr "Permitir dimensionar as janelas maximizadas nos extremos das janelas"

#. i18n: ectx: property (text), widget (QCheckBox, useWindowColors)
#: config/ui/oxygenconfigurationui.ui:144
#, kde-format
msgid "Use the same colors for title bar and window content"
msgstr "Usar as mesmas cores para o título e o conteúdo da janela"

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: config/ui/oxygenconfigurationui.ui:152
#, kde-format
msgid "Animations"
msgstr "Animações"

#. i18n: ectx: attribute (title), widget (QWidget, tab_4)
#: config/ui/oxygenconfigurationui.ui:165
#, kde-format
msgid "Shadows"
msgstr "Sombras"

#. i18n: ectx: property (title), widget (Oxygen::ShadowConfigWidget, activeShadowConfiguration)
#: config/ui/oxygenconfigurationui.ui:171
#, kde-format
msgid "Active Window Glow"
msgstr "Brilho da Janela Activa"

#. i18n: ectx: property (title), widget (Oxygen::ShadowConfigWidget, inactiveShadowConfiguration)
#: config/ui/oxygenconfigurationui.ui:181
#, kde-format
msgid "Window Drop-Down Shadow"
msgstr "Sombreado da Janela"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: config/ui/oxygenconfigurationui.ui:205
#, kde-format
msgid "Window-Specific Overrides"
msgstr "Substituições Específicas da Janela"

#. i18n: ectx: property (windowTitle), widget (QDialog, OxygenExceptionDialog)
#. i18n: ectx: property (windowTitle), widget (QDialog, OxygenDetectWidget)
#: config/ui/oxygendetectwidget.ui:14 config/ui/oxygenexceptiondialog.ui:14
#, kde-format
msgid "Dialog"
msgstr "Janela"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: config/ui/oxygendetectwidget.ui:20
#, kde-format
msgid "Information about Selected Window"
msgstr "Informação Acerca da Janela Seleccionada"

#. i18n: ectx: property (text), widget (QLabel, label)
#: config/ui/oxygendetectwidget.ui:26
#, kde-format
msgid "Class: "
msgstr "Classe: "

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: config/ui/oxygendetectwidget.ui:43
#, kde-format
msgid "Title: "
msgstr "Título: "

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: config/ui/oxygendetectwidget.ui:63
#, kde-format
msgid "Window Property Selection"
msgstr "Selecção da Propriedade da Janela"

#. i18n: ectx: property (text), widget (QRadioButton, windowClassCheckBox)
#: config/ui/oxygendetectwidget.ui:69
#, kde-format
msgid "Use window class (whole application)"
msgstr "Usar a classe da janela (aplicação inteira)"

#. i18n: ectx: property (text), widget (QRadioButton, windowTitleCheckBox)
#: config/ui/oxygendetectwidget.ui:79
#, kde-format
msgid "Use window title"
msgstr "Usar o título da janela"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: config/ui/oxygenexceptiondialog.ui:20
#, kde-format
msgid "Window Identification"
msgstr "Identificação da Janela"

#. i18n: ectx: property (text), widget (QLabel, label)
#: config/ui/oxygenexceptiondialog.ui:26
#, kde-format
msgid "&Matching window property: "
msgstr "Corresponde&nte à propriedade da janela: "

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: config/ui/oxygenexceptiondialog.ui:39
#, kde-format
msgid "Regular expression &to match: "
msgstr "Expressão regular a &corresponder: "

#. i18n: ectx: property (text), widget (QPushButton, detectDialogButton)
#: config/ui/oxygenexceptiondialog.ui:52
#, kde-format
msgid "Detect Window Properties"
msgstr "Detectar as Propriedades da Janela"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: config/ui/oxygenexceptiondialog.ui:83
#, kde-format
msgid "Decoration Options"
msgstr "Opções de Decoração"

#. i18n: ectx: property (text), widget (QCheckBox, borderSizeCheckBox)
#: config/ui/oxygenexceptiondialog.ui:89
#, kde-format
msgid "Border size:"
msgstr "Tamanho do contorno:"

#. i18n: ectx: property (text), widget (QCheckBox, hideTitleBar)
#: config/ui/oxygenexceptiondialog.ui:96
#, kde-format
msgid "Hide window title bar"
msgstr "Esconder a barra de título da janela"

#. i18n: ectx: property (text), item, widget (QComboBox, borderSizeComboBox)
#: config/ui/oxygenexceptiondialog.ui:107
#, kde-format
msgctxt "@item:inlistbox Border size:"
msgid "No Border"
msgstr "Sem Contorno"

#. i18n: ectx: property (text), item, widget (QComboBox, borderSizeComboBox)
#: config/ui/oxygenexceptiondialog.ui:112
#, kde-format
msgctxt "@item:inlistbox Border size:"
msgid "No Side Borders"
msgstr "Sem Contornos Laterais"

#. i18n: ectx: property (text), item, widget (QComboBox, borderSizeComboBox)
#: config/ui/oxygenexceptiondialog.ui:117
#, kde-format
msgctxt "@item:inlistbox Border size:"
msgid "Tiny"
msgstr "Minúsculo"

#. i18n: ectx: property (text), item, widget (QComboBox, borderSizeComboBox)
#: config/ui/oxygenexceptiondialog.ui:122
#, kde-format
msgctxt "@item:inlistbox Border size:"
msgid "Normal"
msgstr "Normal"

#. i18n: ectx: property (text), item, widget (QComboBox, borderSizeComboBox)
#: config/ui/oxygenexceptiondialog.ui:127
#, kde-format
msgctxt "@item:inlistbox Border size:"
msgid "Large"
msgstr "Grande"

#. i18n: ectx: property (text), item, widget (QComboBox, borderSizeComboBox)
#: config/ui/oxygenexceptiondialog.ui:132
#, kde-format
msgctxt "@item:inlistbox Border size:"
msgid "Very Large"
msgstr "Muito Grande"

#. i18n: ectx: property (text), item, widget (QComboBox, borderSizeComboBox)
#: config/ui/oxygenexceptiondialog.ui:137
#, kde-format
msgctxt "@item:inlistbox Border size:"
msgid "Huge"
msgstr "Enorme"

#. i18n: ectx: property (text), item, widget (QComboBox, borderSizeComboBox)
#: config/ui/oxygenexceptiondialog.ui:142
#, kde-format
msgctxt "@item:inlistbox Border size:"
msgid "Very Huge"
msgstr "Mais do que Enorme"

#. i18n: ectx: property (text), item, widget (QComboBox, borderSizeComboBox)
#: config/ui/oxygenexceptiondialog.ui:147
#, kde-format
msgctxt "@item:inlistbox Border size:"
msgid "Oversized"
msgstr "Ainda Maior"

#. i18n: ectx: property (text), widget (QPushButton, moveUpButton)
#: config/ui/oxygenexceptionlistwidget.ui:58
#, kde-format
msgid "Move Up"
msgstr "Subir"

#. i18n: ectx: property (text), widget (QPushButton, moveDownButton)
#: config/ui/oxygenexceptionlistwidget.ui:65
#, kde-format
msgid "Move Down"
msgstr "Descer"

#. i18n: ectx: property (text), widget (QPushButton, addButton)
#: config/ui/oxygenexceptionlistwidget.ui:72
#, kde-format
msgid "Add"
msgstr "Adicionar"

#. i18n: ectx: property (text), widget (QPushButton, editButton)
#: config/ui/oxygenexceptionlistwidget.ui:86
#, kde-format
msgid "Edit"
msgstr "Editar"
